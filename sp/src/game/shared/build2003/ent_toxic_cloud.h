//====== 2021 Created by AnthonyPython for Build2003 ==========================//
//
// Purpose: Entity Toxic Cloud
// Use: Creates a toxic cloud in a sphere that does damage.
//
//=============================================================================//
#ifndef ENT_TOXIC_CLOUD_H
#define ENT_TOXIC_CLOUD_H
#ifdef _WIN32
#pragma once
#endif

#include "cbase.h"


#ifdef GAME_DLL
#include "baseentity.h"
#else
#include "c_baseentity.h"
#endif
#ifdef CLIENT_DLL
#define CEntToxicCloud C_EntToxicCloud
#define CBaseEntity C_BaseEntity
#endif

#define SOUND_EMIT "ambient.steam01"

class CEntToxicCloud : public CBaseEntity
{
public:
	DECLARE_CLASS( CEntToxicCloud, CBaseEntity );
	DECLARE_NETWORKCLASS();

	

	

	bool m_bCloudActive;
#ifndef CLIENT_DLL
	DECLARE_DATADESC();
	~CEntToxicCloud();
	virtual int UpdateTransmitState( void );

public:
	void Spawn();
	void Think_Cloud(void);

	float m_fCloudRadius;
	int m_iDamageAmount;
	float m_fDamageRate;

#else
	CEntToxicCloud()
	{
		m_pCloudEffect = NULL;
	}


	
	virtual void OnDataChanged (DataUpdateType_t updateType );
	CNewParticleEffect *m_pCloudEffect;
#endif
};
#endif