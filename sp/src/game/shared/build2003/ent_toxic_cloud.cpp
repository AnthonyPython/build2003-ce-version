//====== 2021 Created by AnthonyPython for Build2003 ==========================//
//
// Purpose: Entity Toxic Cloud
// Use: Creates a toxic cloud in a sphere that does damage.
//
//=============================================================================//
#include "cbase.h"
#include "engine/IEngineSound.h"
#include "build2003/ent_toxic_cloud.h"


// Server specific.
#ifdef GAME_DLL
#include "hl2_player.h"
#include "soundent.h"
#endif



IMPLEMENT_NETWORKCLASS_ALIASED(EntToxicCloud, DT_CEntToxicCloud)

BEGIN_NETWORK_TABLE(CEntToxicCloud, DT_CEntToxicCloud)
END_NETWORK_TABLE()

#ifndef CLIENT_DLL


BEGIN_DATADESC(CEntToxicCloud)
DEFINE_KEYFIELD(m_fCloudRadius, FIELD_FLOAT, "radius"),
DEFINE_KEYFIELD(m_iDamageAmount, FIELD_INTEGER, "damage"),
DEFINE_KEYFIELD(m_fDamageRate, FIELD_FLOAT, "damagerate"),
DEFINE_THINKFUNC(Think_Cloud),
END_DATADESC()
LINK_ENTITY_TO_CLASS(ent_toxic_cloud, CEntToxicCloud);
#endif

#ifndef CLIENT_DLL

void CEntToxicCloud::Spawn()
{
	EmitSound(SOUND_EMIT);
	//m_fCloudRadius = 120;

	//m_fDamageRate = 0.55;

	//m_iDamageAmount = 5;
	SetThink(&CEntToxicCloud::Think_Cloud);
	SetNextThink(gpGlobals->curtime + 0.1f);
}

CEntToxicCloud::~CEntToxicCloud()
{
	StopSound(SOUND_EMIT);
}
void CEntToxicCloud::Think_Cloud(void)
{
	CBaseEntity* pEntity = NULL;

	for (CEntitySphereQuery sphere(GetAbsOrigin(), m_fCloudRadius); (pEntity = sphere.GetCurrentEntity()) != NULL; sphere.NextEntity())
	{

		if (pEntity->m_takedamage == DAMAGE_NO)
			continue;

		if (pEntity->Classify() == CLASS_ALIEN_FLORA)
		{
			continue;
		}

		// check for a valid player or NPC.
		
		if (pEntity && pEntity->IsPlayer() || pEntity && pEntity->IsNPC())
		{
			CTakeDamageInfo info(this, this, vec3_origin, pEntity->WorldSpaceCenter(), m_iDamageAmount, DMG_NERVEGAS | DMG_PREVENT_PHYSICS_FORCE);
			pEntity->TakeDamage(info);
		}
		else
		{
			continue;
		}
	}

	SetNextThink(gpGlobals->curtime + m_fDamageRate);
}
int CEntToxicCloud::UpdateTransmitState(void)
{
	return SetTransmitState(FL_EDICT_PVSCHECK);
}

#else

void CEntToxicCloud::OnDataChanged(DataUpdateType_t updateType)
{
	if (updateType == DATA_UPDATE_CREATED && m_pCloudEffect == NULL)
	{
		m_pCloudEffect = ParticleProp()->Create("blood_impact_yellow_01", PATTACH_ABSORIGIN);
	}
}

#endif // CLIENT_DLL