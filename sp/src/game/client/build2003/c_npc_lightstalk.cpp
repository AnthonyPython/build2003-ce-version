//----------------
// Purpose: Client Side File for the Lightstalk npc allowing for dynamic light.
//----------------
#include "cbase.h"
#include "c_ai_basenpc.h"
#include "iefx.h"
#include "dlight.h"
#include "view.h"
#include "clientsideeffects.h"
#include "fx.h" //Everything exept Dlight.h and cbase.h are here to just to be sure

class C_NPC_Lightstalk : public C_AI_BaseNPC, CSimpleEmitter
{
public:
	DECLARE_CLASS(C_NPC_Lightstalk, C_AI_BaseNPC);
	DECLARE_CLIENTCLASS();

	//virtual			~C_NPC_Lightstalk(void);
	C_NPC_Lightstalk();

	void	Spawn(void);
	void	Update(float timeDelta);
	//void	NotifyDestroyParticle(Particle* pParticle);
	void	RestoreResources(void);

	bool	m_bLight;
	float	m_flScale;
	pixelvis_handle_t m_queryHandle;


private:
	C_NPC_Lightstalk(const C_NPC_Lightstalk &);

	int		m_iAttachment;

	//SimpleParticle	*m_pParticle[2];
};

IMPLEMENT_CLIENTCLASS_DT(C_NPC_Lightstalk, DT_Lightstalk, CNPC_LightStalk)
RecvPropFloat(RECVINFO(m_flScale)),
RecvPropInt(RECVINFO(m_bLight)),
END_RECV_TABLE()


//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
C_NPC_Lightstalk::C_NPC_Lightstalk() : CSimpleEmitter("C_NPC_Lightstalk")
{
	
	
	//m_pParticle[0] = NULL;
	//m_pParticle[1] = NULL;

	m_bLight = true;

	SetDynamicallyAllocated(false);
	m_queryHandle = 0;

	m_iAttachment = -1;
}


void C_NPC_Lightstalk::Spawn()
{
	Msg("I just spawned :)\n");
	m_bLight = true;
}

// Update
void C_NPC_Lightstalk::Update( float timeDelta )
{
	if (!IsVisible())
		return;

	CSimpleEmitter::Update(timeDelta);

	//Don't do this if the console is down
	if (timeDelta <= 0.0f)
		return;

	//
	// Dynamic light
	//

	float	baseScale = m_flScale;

	if (m_bLight)
	{
		//SetEFlags(EF_NOINTERP);

		



		if (m_iAttachment == -1)
		{
			m_iAttachment = LookupAttachment("Lip");
		}

		if (m_iAttachment != -1)
		{

			Vector effect_origin;
			QAngle effect_angles;


			GetAttachment(m_iAttachment, effect_origin, effect_angles);

			dlight_t *dl = effects->CL_AllocDlight(index);
			//Raise the light a little bit away from the Attachment so it lights it up better. EDIT: Don't do this for the lightstalk set it to 0
			dl->origin = effect_origin + Vector(0, 0, 0);
			dl->color.r = 255;
			dl->color.g = 255;
			dl->color.b = 255;
			dl->die = gpGlobals->curtime + 0.10f; //0.05 normally 0.10f
			dl->radius = baseScale * 64.0f; //* random->RandomFloat(260.0f, 290.0f); This is way too big o_o
			//dl->decay = 512.0f; //Don't Decay this is done on the serverside

		}
	}
}