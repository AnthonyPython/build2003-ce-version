
#include "cbase.h"
#include "ai_basenpc.h"
#include "npc_baseflora.h"
#include "build2003/ent_toxic_cloud.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

class CNPC_AlienFauna1 : public CNPC_BaseFlora
{
	DECLARE_CLASS(CNPC_AlienFauna1, CNPC_BaseFlora);
	DECLARE_DATADESC();

	~CNPC_AlienFauna1();

public:
	void	Spawn(void);
	void Precache();
	Class_T			Classify(void);
	void	CreateToxicCloud();
	virtual void		Event_Killed(const CTakeDamageInfo& info);
	float m_fCloudRadius;
	int m_iDamageAmount;
	float  m_fDamageRate;

	CHandle<CEntToxicCloud> m_hToxicCloudEffect;

};

BEGIN_DATADESC(CNPC_AlienFauna1)
DEFINE_KEYFIELD(m_fCloudRadius, FIELD_FLOAT,"radius"),
DEFINE_KEYFIELD(m_iDamageAmount, FIELD_INTEGER, "damage"),
DEFINE_KEYFIELD(m_fDamageRate, FIELD_FLOAT, "damagerate"),
END_DATADESC()

LINK_ENTITY_TO_CLASS(npc_alienfauna1, CNPC_AlienFauna1);


CNPC_AlienFauna1::~CNPC_AlienFauna1()
{
	if (m_hToxicCloudEffect.Get())
	{
		UTIL_Remove(m_hToxicCloudEffect);
	}
	StopSound(SOUND_EMIT);
}

//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CNPC_AlienFauna1::Classify(void)
{
	return	CLASS_ALIEN_FLORA;
}

ConVar sk_alienfauna1_health("sk_alienfauna1_health", "100");


void CNPC_AlienFauna1::Event_Killed(const CTakeDamageInfo& info)
{
	BaseClass::Event_Killed(info);

	if (m_hToxicCloudEffect.Get())
	{
		UTIL_Remove(m_hToxicCloudEffect);
	}
	StopSound(SOUND_EMIT);
}
void CNPC_AlienFauna1::Precache()
{
	if (!GetModelName())
	{
		// Level designer has not provided a default
		SetModelName(MAKE_STRING("models/npcs/alien_fauna.mdl"));
	}
	PrecacheModel(STRING(GetModelName()));
	PrecacheParticleSystem("blood_impact_yellow_01");
	PrecacheScriptSound(SOUND_EMIT);
	BaseClass::Precache();
}

void CNPC_AlienFauna1::Spawn()
{
	Precache();
	BaseClass::Spawn();
	//SetHullType(HULL_TINY);
	// Hairs don't collide
	//SetSolid(SOLID_BBOX);
	SetGravity(0.0f);

	m_iMaxHealth = sk_alienfauna1_health.GetFloat();
	m_iHealth = m_iMaxHealth;
	SetModel(STRING(GetModelName()));

	m_hToxicCloudEffect = NULL;

	//Must of spawned either via console or mapper forgot to set defaults above 0.1
	if(m_fCloudRadius < 0.1)
		m_fCloudRadius = 120;

	if (m_fDamageRate < 0.1)
		m_fDamageRate = 0.55;

	if (m_iDamageAmount < 0.1)
		m_iDamageAmount = 5;

	CreateToxicCloud();


}

void CNPC_AlienFauna1::CreateToxicCloud()
{
	m_hToxicCloudEffect = (CEntToxicCloud*)CreateEntityByName("ent_toxic_cloud");

	CEntToxicCloud* p_ToxicCloud = m_hToxicCloudEffect.Get();

	if (p_ToxicCloud)
	{
		DispatchSpawn(p_ToxicCloud);
		p_ToxicCloud->m_fCloudRadius = m_fCloudRadius;
		p_ToxicCloud->m_iDamageAmount = m_iDamageAmount;
		p_ToxicCloud->m_fDamageRate = m_fDamageRate;
		p_ToxicCloud->SetAbsOrigin(GetAbsOrigin());
		p_ToxicCloud->SetParent(this);
	}

}