//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: Implements the hl1 scientist
//
// $NoKeywords: $
// Recreated by anthony python
//=============================================================================//

#include "cbase.h"
#include "game.h"
#include "AI_Default.h"
#include "AI_Schedule.h"
#include "AI_Hull.h"
#include "AI_Navigator.h"
#include "AI_Motor.h"
#include "ai_squad.h"
#include "ai_route.h"
#include "ai_pathfinder.h"
#include "npc_scientist.h"
#include "npcevent.h"
#include "soundent.h"
#include "activitylist.h"
#include "npc_headcrab.h"
#include "player.h"
#include "gamerules.h"		// For g_pGameRules
#include "ammodef.h"
#include "entitylist.h"
#include "shake.h"
#include "vstdlib/random.h"
#include "engine/IEngineSound.h"
#include "movevars_shared.h"
#include "hl2_shareddefs.h"
#include "soundemittersystem/isoundemittersystembase.h"
#include "AI_Hint.h"
#include "AI_Senses.h"


//ConVar sk_scientist_health("sk_scientist_health", "20");
//ConVar sk_scientist_heal("sk_scientist_heal", "25");

#define		NUM_SCIENTIST_HEADS		4 // four heads available for scientist model
enum { HEAD_GLASSES = 0, HEAD_EINSTEIN = 1, HEAD_LUTHER = 2, HEAD_SLICK = 3 };

class CBarney : public  CNPCSimpleTalker
{
	DECLARE_CLASS(CBarney, CNPCSimpleTalker);
	DECLARE_DATADESC();
public:
	void Spawn(void);
	virtual void Precache(void);
	Class_T	Classify(void);

	//virtual void FollowerUse(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value);
	//void IdleSound(void);
	void PainSound(const CTakeDamageInfo& info);
	//virtual void AlertSound(void);
	void DeathSound(const CTakeDamageInfo& info);

	BOOL CheckRangeAttack1(float flDot, float flDist);

	BOOL IsFacing(CBaseEntity* pEntity, const Vector& reference);

	virtual float MaxYawSpeed(void);

	void HandleAnimEvent(animevent_t* pEvent);


	int OnTakeDamage_Alive(const CTakeDamageInfo& inputInfo);

	int GetSoundInterests(void);
	//void RunAI(void);
	//virtual void OnListened(void);

	virtual int SelectSchedule(void);
	virtual void TraceAttack(const CTakeDamageInfo& info, const Vector& vecDir, trace_t* ptr, CDmgAccumulator* pAccumulator);


	virtual void DeclineFollowing(void);

	virtual Activity GetStoppedActivity(void);

	virtual void Event_Killed(const CTakeDamageInfo& inputInfo);

	virtual NPC_STATE SelectIdealState(void);

	void BarneyFirePistol(void);


	virtual void FollowerUse(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value);

	void StartTask(const Task_t* pTask);
	void RunTask(const Task_t* pTask);

	BOOL	m_fGunDrawn;
	float	m_checkAttackTime;
	BOOL	m_lastAttackCheck;
	void TalkInit(void);

	int		m_iAmmoType;

	enum
	{
		SCHED_BARNEY_FOLLOW = BaseClass::NEXT_SCHEDULE,
		SCHED_BARNEY_ENEMY_DRAW,
		SCHED_BARNEY_FACE_TARGET,
		SCHED_BARNEY_IDLE_STAND,
		SCHED_BARNEY_STOP_FOLLOWING,
	};


	virtual int TranslateSchedule(int scheduleType);

	BOOL	DisregardEnemy(CBaseEntity* pEnemy) { return !pEnemy->IsAlive() || (gpGlobals->curtime - m_fearTime) > 15; }


	//virtual Activity NPC_TranslateActivity(Activity newActivity);


	//Network Later! Better do it soon! I mean it! am typing this at 5:45AM, god damn it! --AnthonyP
	int m_nBody;


	virtual void ModifyOrAppendCriteria(AI_CriteriaSet& criteriaSet);

	DEFINE_CUSTOM_AI;

	float TargetDistance(void);

private:
	float m_painTime;
	float m_healTime;
	float m_fearTime;
};

//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		SCIENTIST_AE_HEAL		( 1 )
#define		SCIENTIST_AE_NEEDLEON	( 2 )
#define		SCIENTIST_AE_NEEDLEOFF	( 3 )

#define		BARNEY_AE_DRAW		( 2 )
#define		BARNEY_AE_SHOOT		( 3 )
#define		BARNEY_AE_HOLSTER	( 4 )

#define	BARNEY_BODY_GUNHOLSTERED	0
#define	BARNEY_BODY_GUNDRAWN		1
#define BARNEY_BODY_GUNGONE			2

//=======================================================
// Scientist
//=======================================================

#ifdef HL1_NPC
LINK_ENTITY_TO_CLASS(monster_barney, CBarney);
#endif



//=========================================================
// Monster's Anim Events Go Here
//=========================================================
#define		SCIENTIST_AE_HEAL		( 1 )
#define		SCIENTIST_AE_NEEDLEON	( 2 )
#define		SCIENTIST_AE_NEEDLEOFF	( 3 )


BEGIN_DATADESC(CBarney)

DEFINE_FIELD(m_painTime, FIELD_TIME),
DEFINE_FIELD(m_healTime, FIELD_TIME),
DEFINE_FIELD(m_fearTime, FIELD_TIME),
DEFINE_FIELD(m_fGunDrawn, FIELD_BOOLEAN),
DEFINE_FIELD(m_painTime, FIELD_TIME),
DEFINE_FIELD(m_checkAttackTime, FIELD_TIME),
DEFINE_FIELD(m_lastAttackCheck, FIELD_BOOLEAN),
END_DATADESC()


void CBarney::Precache(void)
{
	PrecacheModel("models/barney.mdl");
	PrecacheScriptSound("npc_Scientist.Pain");


	TalkInit();
	BaseClass::Precache();
}

void CBarney::DeclineFollowing(void)
{
	GetExpresser()->BlockSpeechUntil(gpGlobals->curtime + 10);
	//Talk(10);
	SetSpeechTarget(GetEnemy());
	//m_hTalkTarget = m_hEnemy;
	//Speak("SC_POK");
	PlaySentence("BA_POK", 2, VOL_NORM, SNDLVL_60dB);
}

void CBarney::TraceAttack(const CTakeDamageInfo& info, const Vector& vecDir, trace_t* ptr, CDmgAccumulator* pAccumulator)
{
	float flDamage = info.GetDamage();
	switch (ptr->hitgroup)
	{
	case HITGROUP_CHEST:
	case HITGROUP_STOMACH:
		if (info.GetDamageType() & (DMG_BULLET | DMG_SLASH | DMG_BLAST))
		{
			//info.set
			flDamage = flDamage / 2;
		}
		break;
	case 10:
		if (info.GetDamageType() & (DMG_BULLET | DMG_SLASH | DMG_CLUB))
		{
			flDamage -= 20;
			if (flDamage <= 0)
			{
				//UTIL_Ricochet(ptr->vecEndPos, 1.0);
				flDamage = 0.01;
			}
		}
		// always a head shot
		ptr->hitgroup = HITGROUP_HEAD;
		break;
	}
	CTakeDamageInfo endresult(info.GetInflictor(), info.GetAttacker(), flDamage, 1.0f, info.GetDamageType());
	//(const CTakeDamageInfo & info, const Vector & vecDir, trace_t * ptr, CDmgAccumulator * pAccumulator)

	BaseClass::TraceAttack(endresult, vecDir, ptr, NULL);
}

Activity CBarney::GetStoppedActivity(void)
{
	//if (GetEnemy() != NULL)
	//	return (Activity)ACT_EXCITED;
	return BaseClass::GetStoppedActivity();
}





//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CBarney::Classify(void)
{
	return	CLASS_PLAYER_ALLY;//CLASS_HUMAN_PASSIVE;
}

//=========================================================
// SetYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================
float CBarney::MaxYawSpeed(void)
{
	float flYS = 0;

	switch (GetActivity())
	{
	case	ACT_IDLE:			flYS = 120;	break;
	case	ACT_WALK:			flYS = 180;	break;
	case	ACT_RUN:			flYS = 150;	break;
	case ACT_TURN_LEFT:
	case ACT_TURN_RIGHT:
		flYS = 120;
		break;

	default:
		flYS = 90;
		break;
	}

	return flYS;
}


//=========================================================
// CheckRangeAttack1
//=========================================================
BOOL CBarney::CheckRangeAttack1(float flDot, float flDist)
{
	if (flDist <= 1024 && flDot >= 0.5)
	{
		if (gpGlobals->curtime > m_checkAttackTime)
		{
			trace_t tr;

			Vector shootOrigin = GetAbsOrigin() + Vector(0, 0, 55);
			CBaseEntity* pEnemy = GetEnemy();

			Vector shootTarget = ((pEnemy->BodyTarget(shootOrigin) - pEnemy->GetAbsOrigin()) + pEnemy->GetAbsOrigin());
			//UTIL_TraceLine(shootOrigin, shootTarget, dont_ignore_monsters, ENT(pev), &tr);
			UTIL_TraceLine(shootOrigin, shootTarget, NULL, NULL, &tr);
			m_checkAttackTime = gpGlobals->curtime + 1;


			if (tr.fraction == 1.0 || (tr.m_pEnt != NULL && tr.m_pEnt == pEnemy))
				m_lastAttackCheck = TRUE;
			else
				m_lastAttackCheck = FALSE;
			m_checkAttackTime = gpGlobals->curtime + 1.5;
		}
		return m_lastAttackCheck;
	}
	return FALSE;
}
//=========================================================
// BarneyFirePistol - shoots one round from the pistol at
// the enemy barney is facing.
//=========================================================
void CBarney::BarneyFirePistol(void)
{
	Vector vecShootOrigin;

	//UTIL_MakeVectors(pev->angles);
	vecShootOrigin = GetLocalOrigin() + Vector(0, 0, 55);
	Vector vecShootDir = GetShootEnemyDir(vecShootOrigin);

	QAngle angDir;
	VectorAngles(vecShootDir, angDir);
	SetPoseParameter(0, angDir.x);
	//pev->effects = EF_MUZZLEFLASH;
																			//BULLET_MONSTER_9MM
	FireBullets(1, vecShootOrigin, vecShootDir, VECTOR_CONE_2DEGREES, 1024, 2);



	//not sure how to pitch shift this sound atm disabled --AnthonyP

	/*
	int pitchShift = random->RandomInt(0, 20);

	// Only shift about half the time
	if (pitchShift > 10)
		pitchShift = 0;
	else
		pitchShift -= 5;
	EmitSound(this, CHAN_WEAPON, "barney/ba_attack2.wav", 1, ATTN_NORM, 0, 100 + pitchShift);
	*/
	EmitSound("hl1_barney.attack");
	CSoundEnt::InsertSound(SOUND_COMBAT, GetAbsOrigin(), 384, 0.3);

	// UNDONE: Reload?
	m_cAmmoLoaded--;// take away a bullet!
}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//
// Returns number of events handled, 0 if none.
//=========================================================
void CBarney::HandleAnimEvent(animevent_t* pEvent)
{
	switch (pEvent->event)
	{
	case BARNEY_AE_SHOOT:
		BarneyFirePistol();
		break;

	case BARNEY_AE_DRAW:
		// barney's bodygroup switches here so he can pull gun from holster
		m_nBody = BARNEY_BODY_GUNDRAWN;
		SetBodygroup(0, m_nBody);
		m_fGunDrawn = TRUE;
		break;

	case BARNEY_AE_HOLSTER:
		// change bodygroup to replace gun in holster
		m_nBody = BARNEY_BODY_GUNHOLSTERED;
		SetBodygroup(0, m_nBody);
		m_fGunDrawn = FALSE;
		break;

	default:
		BaseClass::HandleAnimEvent(pEvent);
	}
}

//=========================================================
// AI Schedules Specific to this monster
//=========================================================

int CBarney::TranslateSchedule(int scheduleType)
{
	switch (scheduleType)
	{
	case SCHED_ARM_WEAPON:
		if (GetEnemy() != NULL)
		{
			// face enemy, then draw.
			return SCHED_BARNEY_ENEMY_DRAW;
		}
		break;

		// Hook these to make a looping schedule
	case SCHED_TARGET_FACE:
	{
		int	baseType;

		// call base class default so that scientist will talk
		// when 'used' 
		baseType = BaseClass::TranslateSchedule(scheduleType);

		if (baseType == SCHED_IDLE_STAND)
			return SCHED_BARNEY_FACE_TARGET;
		else
			return baseType;
	}
	break;

	case SCHED_TARGET_CHASE:
	{
		return SCHED_BARNEY_FOLLOW;
		break;
	}

	case SCHED_IDLE_STAND:
	{
		int	baseType;

		// call base class default so that scientist will talk
		// when 'used' 
		baseType = BaseClass::TranslateSchedule(scheduleType);

		if (baseType == SCHED_IDLE_STAND)
			return SCHED_BARNEY_IDLE_STAND;
		else
			return baseType;
	}
	break;

	case SCHED_TAKE_COVER_FROM_ENEMY:
	case SCHED_CHASE_ENEMY:
	{
		if (HasCondition(COND_HEAVY_DAMAGE))
			return SCHED_TAKE_COVER_FROM_ENEMY;

		// No need to take cover since I can see him
		// SHOOT!
		if (HasCondition(COND_CAN_RANGE_ATTACK1) && m_fGunDrawn)
			return SCHED_RANGE_ATTACK1;
	}
	break;
	}

	return BaseClass::TranslateSchedule(scheduleType);
}

//------------------------------------------------------------------------------
// Purpose : For innate range attack
// Input   :
// Output  :
//------------------------------------------------------------------------------
/*int CBarney::RangeAttack1Conditions(float flDot, float flDist)
{
	if (GetEnemy() == NULL)
	{
		return(COND_NONE);
	}

	else if (flDist > 1024)
	{
		return(COND_TOO_FAR_TO_ATTACK);
	}
	else if (flDot < 0.5)
	{
		return(COND_NOT_FACING_ATTACK);
	}

	if (CheckRangeAttack1(flDot, flDist))
		return(COND_CAN_RANGE_ATTACK1);

	return COND_NONE;
}
*/
void CBarney::FollowerUse(CBaseEntity* pActivator, CBaseEntity* pCaller, USE_TYPE useType, float value)
{
	// Don't allow use during a scripted_sentence
	if (m_useTime > gpGlobals->curtime)
		return;

	if (pCaller != NULL && pCaller->IsPlayer())
	{
		if (!m_FollowBehavior.GetFollowTarget() && IsInterruptable())
		{
#if TOML_TODO
			LimitFollowers(pCaller, 1);
#endif

			if (m_afMemory & bits_MEMORY_PROVOKED)
				Msg("I'm not following you, you evil person!\n");
			else
			{

				StartFollowing(pCaller);
				Speak(TLK_STARTFOLLOW);
				SetSpeechTarget(GetTarget());
				ClearCondition(COND_PLAYER_PUSHING);
			}
		}
		else
		{
			StopFollowing();
			Speak(TLK_STOPFOLLOW);
		}
	}
}

//=========================================================
// Spawn
//=========================================================
void CBarney::Spawn(void)
{
	Precache();
	m_nBody = -1;
	SetModel("models/barney.mdl");
	SetHullType(HULL_HUMAN);
	SetHullSizeNormal();
	//UTIL_SetSize(pev, HUMAN_HULL_MIN, HUMAN_HULL_MAX);

	SetSolid(SOLID_BBOX);
	SetMoveType(MOVETYPE_STEP);
	m_bloodColor = BLOOD_COLOR_RED;
	AddSolidFlags(FSOLID_NOT_STANDABLE);

	m_iHealth = 50;// sk_scientist_health.GetFloat();

	m_flFieldOfView = VIEW_FIELD_WIDE; // NOTE: we need a wide field of view so scientists will notice player and say hello
	m_NPCState = NPC_STATE_NONE;

	//	m_flDistTooFar		= 256.0;

	CapabilitiesClear();
	CapabilitiesAdd(bits_CAP_MOVE_GROUND | bits_CAP_ANIMATEDFACE | bits_CAP_TURN_HEAD | bits_CAP_OPEN_DOORS | bits_CAP_AUTO_DOORS | bits_CAP_USE | bits_CAP_WEAPON_RANGE_ATTACK1);

	// White hands
	m_nSkin = 0;

	if (m_nBody == -1)
	{// -1 chooses a random head
		m_nBody = random->RandomInt(0, NUM_SCIENTIST_HEADS - 1);// pick a head, any head

		//SetBodygroup(1, m_nBody);
	}

	// Luther is black, make his hands black
	//if (m_nBody == HEAD_LUTHER)
	//	m_nSkin = 1;

	NPCInit();
	SetUse(&CBarney::FollowerUse);
}

// Init talk data
void CBarney::TalkInit()
{

	BaseClass::TalkInit();

	// scientist will try to talk to friends in this order:

	m_szFriends[0] = "monster_scientist";
	m_szFriends[1] = "monster_sitting_scientist";
	m_szFriends[2] = "monster_barney";
	m_szFriends[3] = "npc_scientist";
	m_szFriends[4] = "npc_sitting_scientist";
	m_szFriends[5] = "npc_barney";
	m_szFriends[6] = "npc_citizen";
	m_szFriends[7] = "npc_crow";

	// get voice for head
	switch (m_nBody % 3)
	{
	default:
	case HEAD_GLASSES:	GetExpresser()->SetVoicePitch(105); break;	//glasses
	case HEAD_EINSTEIN: GetExpresser()->SetVoicePitch(100); break;	//einstein
	case HEAD_LUTHER:	GetExpresser()->SetVoicePitch(95);  break;	//luther
	case HEAD_SLICK:	GetExpresser()->SetVoicePitch(100);  break;//slick
	}
}

int CBarney::OnTakeDamage_Alive(const CTakeDamageInfo& inputInfo)
{
	CBaseEntity* pevInflictor = inputInfo.GetInflictor();

	if (pevInflictor && pevInflictor->IsPlayer())
	{
		Remember(bits_MEMORY_PROVOKED);
		StopFollowing();
	}

	// make sure friends talk about it if player hurts scientist...
	return BaseClass::OnTakeDamage_Alive(inputInfo);
}


void CBarney::ModifyOrAppendCriteria(AI_CriteriaSet& criteriaSet)
{
	bool IsitBeforeDisater;
	if (m_spawnflags & SF_BEFOREDISATER)
	{
		IsitBeforeDisater = true;
	}
	else
	{
		IsitBeforeDisater = false;
	}

	criteriaSet.AppendCriteria("disaster", IsitBeforeDisater ? "[disaster::pre]" : "[disaster::post]");

	BaseClass::ModifyOrAppendCriteria(criteriaSet);

}

//=========================================================
// ISoundMask - returns a bit mask indicating which types
// of sounds this monster regards. In the base class implementation,
// monsters care about all sounds, but no scents.
//=========================================================
int CBarney::GetSoundInterests(void)
{
	return	SOUND_WORLD |
		SOUND_COMBAT |
		SOUND_DANGER |
		SOUND_PLAYER;
}

//=========================================================
// PainSound
//=========================================================
void CBarney::PainSound(const CTakeDamageInfo& inputInfo)
{
	if (gpGlobals->curtime < m_painTime)
		return;

	m_painTime = gpGlobals->curtime + random->RandomFloat(0.5, 0.75);

	CSoundParameters param;
	CPASAttenuationFilter thisEntity(this);
	if (GetParametersForSound("npc_Scientist.Pain", param, NULL))
	{
		EmitSound_t snd(param);
		param.pitch = GetExpresser()->GetVoicePitch();
		EmitSound(thisEntity, entindex(), snd);
	}

	// not needed anymore but this use to be limited by 5 sounds for a pain sound, no more!
	/*switch (random->RandomInt(0, 4))
	{
	case 0: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain1.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
	case 1: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain2.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
	case 2: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain3.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
	case 3: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain4.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
	case 4: EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "scientist/sci_pain5.wav", 1, ATTN_NORM, 0, GetVoicePitch()); break;
	}*/
}

//=========================================================
// DeathSound 
//=========================================================
void CBarney::DeathSound(const CTakeDamageInfo& inputInfo)
{
	PainSound(inputInfo);
}


void CBarney::Event_Killed(const CTakeDamageInfo& inputInfo)
{
	if (m_nBody < BARNEY_BODY_GUNGONE)
	{// drop the gun!
		Vector vecGunPos;

		QAngle QGunAngles = QAngle(0, 0, 0);

		m_nBody = BARNEY_BODY_GUNGONE;

		int weaponBone = this->LookupAttachment("0");

		if (weaponBone != -1)
		{
			GetAttachment(weaponBone, vecGunPos, QGunAngles);

		}

		//GetAttachment(0, vecGunPos, vecGunAngles);

		//CBaseEntity* pGun =
		DropItem("weapon_pistol", vecGunPos, QGunAngles);
	}

	SetUse(NULL);
	BaseClass::Event_Killed(inputInfo);
}

//=========================================================
// SelectSchedule - Decides which type of schedule best suits
// the monster's current state and conditions. Then calls
// monster's member function to get a pointer to a schedule
// of the proper type.
//=========================================================
int CBarney::SelectSchedule(void)
{
	if (m_NPCState == NPC_STATE_COMBAT || GetEnemy() != NULL)
	{
		// Priority action!
		if (!m_fGunDrawn)
			return SCHED_ARM_WEAPON;
	}

	if (GetFollowTarget() == NULL)
	{
		if (HasCondition(COND_PLAYER_PUSHING) /* && !(GetSpawnFlags() & SF_NPC_PREDISASTER)*/)	// Player wants me to move
			return SCHED_MOVE_AWAY;
	}

	if (BehaviorSelectSchedule())
		return BaseClass::SelectSchedule();

	if (HasCondition(COND_HEAR_DANGER))
	{
		CSound* pSound;
		pSound = GetBestSound();

		ASSERT(pSound != NULL);

		if (pSound && pSound->IsSoundType(SOUND_DANGER))
			return SCHED_TAKE_COVER_FROM_BEST_SOUND;
	}
	if (HasCondition(COND_ENEMY_DEAD) && IsOkToSpeak())
	{
		//Speak(BA_KILL);
	}

	switch (m_NPCState)
	{
	case NPC_STATE_COMBAT:
	{
		// dead enemy
		if (HasCondition(COND_ENEMY_DEAD))
			return BaseClass::SelectSchedule(); // call base class, all code to handle dead enemies is centralized there.

	   // always act surprized with a new enemy
		if (HasCondition(COND_NEW_ENEMY) && HasCondition(COND_LIGHT_DAMAGE))
			return SCHED_SMALL_FLINCH;

		if (HasCondition(COND_HEAVY_DAMAGE))
			return SCHED_TAKE_COVER_FROM_ENEMY;

		if (!HasCondition(COND_SEE_ENEMY))
		{
			// we can't see the enemy
			if (!HasCondition(COND_ENEMY_OCCLUDED))
			{
				// enemy is unseen, but not occluded!
				// turn to face enemy
				return SCHED_COMBAT_FACE;
			}
			else
			{
				return SCHED_CHASE_ENEMY;
			}
		}
	}
	break;

	case NPC_STATE_ALERT:
	case NPC_STATE_IDLE:
		if (HasCondition(COND_LIGHT_DAMAGE) || HasCondition(COND_HEAVY_DAMAGE))
		{
			// flinch if hurt
			return SCHED_SMALL_FLINCH;
		}

		if (GetEnemy() == NULL && GetFollowTarget())
		{
			if (!GetFollowTarget()->IsAlive())
			{
				// UNDONE: Comment about the recently dead player here?
				StopFollowing();
				break;
			}
			else
			{

				return SCHED_TARGET_FACE;
			}
		}

		// try to say something about smells
		TrySmellTalk();
		break;
	}

	return BaseClass::SelectSchedule();
}


float CBarney::TargetDistance(void)
{
	CBaseEntity* pTargetWeareFollowing = GetFollowTarget();
	// If we lose the player, or he dies, return a really large distance
	if (pTargetWeareFollowing == NULL || !pTargetWeareFollowing->IsAlive())
		return 1e6;

	return (pTargetWeareFollowing->GetAbsOrigin() - GetAbsOrigin()).Length();
}




void CBarney::StartTask(const Task_t* pTask)
{
	BaseClass::StartTask(pTask);
}

void CBarney::RunTask(const Task_t* pTask)
{
	switch (pTask->iTask)
	{
	case TASK_RANGE_ATTACK1:
		if (GetEnemy() != NULL && (GetEnemy()->IsPlayer()))
		{
			m_flPlaybackRate = 1.5;
		}
		BaseClass::RunTask(pTask);
		break;
	default:
		BaseClass::RunTask(pTask);
		break;
	}
}

NPC_STATE CBarney::SelectIdealState(void)
{
	return BaseClass::SelectIdealState();
}



//------------------------------------------------------------------------------
//
// Schedules
//
//------------------------------------------------------------------------------

AI_BEGIN_CUSTOM_NPC(monster_barney, CBarney)

//=========================================================
// > SCHED_BARNEY_FOLLOW
//=========================================================
DEFINE_SCHEDULE
(
	SCHED_BARNEY_FOLLOW,

	"	Tasks"
	//		"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_BARNEY_STOP_FOLLOWING"
	"		TASK_GET_PATH_TO_TARGET			0"
	"		TASK_MOVE_TO_TARGET_RANGE		180"
	"		TASK_SET_SCHEDULE				SCHEDULE:SCHED_TARGET_FACE"
	"	"
	"	Interrupts"
	"			COND_NEW_ENEMY"
	"			COND_LIGHT_DAMAGE"
	"			COND_HEAVY_DAMAGE"
	"			COND_HEAR_DANGER"
	"			COND_PROVOKED"
)

//=========================================================
// > SCHED_BARNEY_ENEMY_DRAW
//=========================================================
DEFINE_SCHEDULE
(
	SCHED_BARNEY_ENEMY_DRAW,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_FACE_ENEMY				0"
	"		TASK_PLAY_SEQUENCE_FACE_ENEMY		ACTIVITY:ACT_ARM"
	"	"
	"	Interrupts"
)

//=========================================================
// > SCHED_BARNEY_FACE_TARGET
//=========================================================
DEFINE_SCHEDULE
(
	SCHED_BARNEY_FACE_TARGET,

	"	Tasks"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_FACE_TARGET			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_SET_SCHEDULE			SCHEDULE:SCHED_BARNEY_FOLLOW"
	"	"
	"	Interrupts"
	"		COND_GIVE_WAY"
	"		COND_NEW_ENEMY"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_PROVOKED"
	"		COND_HEAR_DANGER"
)

//=========================================================
// > SCHED_BARNEY_IDLE_STAND
//=========================================================
DEFINE_SCHEDULE
(
	SCHED_BARNEY_IDLE_STAND,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
	"		TASK_WAIT					2"
	"		TASK_TALKER_HEADRESET		0"
	"	"
	"	Interrupts"
	"		COND_NEW_ENEMY"
	"		COND_LIGHT_DAMAGE"
	"		COND_HEAVY_DAMAGE"
	"		COND_PROVOKED"
	"		COND_HEAR_COMBAT"
	"		COND_SMELL"
)

AI_END_CUSTOM_NPC()
