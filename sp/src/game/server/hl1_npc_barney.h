#pragma once
#ifndef NPC_BARNEY_H
#define NPC_BARNEY_H

#include "npc_talker.h"

//=========================================================
//=========================================================
class CNPC_HL1_Barney : public CNPCSimpleTalker
{
	DECLARE_CLASS(CNPC_HL1_Barney, CNPCSimpleTalker);
public:

	DECLARE_DATADESC();

	virtual void ModifyOrAppendCriteria(AI_CriteriaSet& set);

	virtual void	Precache(void);
	virtual void	Spawn(void);
	virtual void	TalkInit(void);

	virtual void	StartTask(const Task_t* pTask);
	virtual void	RunTask(const Task_t* pTask);

	virtual int		GetSoundInterests(void);
	virtual Class_T  Classify(void);
	virtual void	AlertSound(void);
	virtual void    SetYawSpeed(void);

	virtual bool    CheckRangeAttack1(float flDot, float flDist);
	virtual void    BarneyFirePistol(void);

	virtual int		OnTakeDamage_Alive(const CTakeDamageInfo& inputInfo);
	virtual void TraceAttack(const CTakeDamageInfo& info, const Vector& vecDir, trace_t* ptr, CDmgAccumulator* pAccumulator);
	virtual void	Event_Killed(const CTakeDamageInfo& info);

	virtual void    PainSound(void);
	virtual void	DeathSound(void);

	virtual void	HandleAnimEvent(animevent_t* pEvent);
	virtual int		TranslateSchedule(int scheduleType);
	virtual int		SelectSchedule(void);

	virtual void	DeclineFollowing(void);

	 int		RangeAttack1Conditions(float flDot, float flDist);

	virtual NPC_STATE SelectIdealState(void);

	bool	m_fGunDrawn;
	float	m_flPainTime;
	float	m_flCheckAttackTime;
	bool	m_fLastAttackCheck;

	int		m_iAmmoType;

	enum
	{
		SCHED_BARNEY_FOLLOW = BaseClass::NEXT_SCHEDULE,
		SCHED_BARNEY_ENEMY_DRAW,
		SCHED_BARNEY_FACE_TARGET,
		SCHED_BARNEY_IDLE_STAND,
		SCHED_BARNEY_STOP_FOLLOWING,
	};

	DEFINE_CUSTOM_AI;
};

#endif	//NPC_BARNEY_H